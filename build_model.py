"""
ネットワーク構築
"""

from keras.layers import Input, Dense, Reshape, Flatten, Dropout, Add
from keras.layers import BatchNormalization, Activation, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.utils import np_utils
from keras.layers import Concatenate
from keras.layers import Lambda
from keras.layers import MaxPooling2D, AveragePooling2D
from keras.layers import GlobalMaxPooling2D, GlobalAveragePooling2D
from keras import initializers
from keras import backend as K

import functools


# 論文通りに CNN を構築
def build_CNN_signfi(input_tensor, output_channels):
    x = input_tensor

    x = Conv2D(3, kernel_size=3, strides=1, padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = AveragePooling2D((3, 3), strides=(3, 3))(x)
    x = Dropout(0.6)(x)
    x = Dense(output_channels)(x)
    x = Activation("softmax")(x)

    output_tensor = x

    model = Model(inputs=input_tensor, outputs=output_tensor)

    return model


# CNN を構築
def build_CNN(input_tensor, output_channels, num_resblock=1, num_conv=1):
    def conv(x, f):
        x = Conv2D(f, kernel_size=3, strides=1, padding='same')(x)
        x = BatchNormalization()(x)
        x = Activation("relu")(x)
        x = MaxPooling2D((2, 2), strides=(2, 2))(x)
        x = Dropout(0.5)(x)

        return x
    def conv_2(x, f):
        x = Conv2D(f, kernel_size=3, strides=1, padding='same')(x)
        x = BatchNormalization()(x)
        x = Activation("relu")(x)

        return x
    # https://deepage.net/deep_learning/2016/11/30/resnet.html
    # 多分これを参考にした
    def resblock(x, f):
        layer_input = x
        x = BatchNormalization()(x)
        x = Activation("relu")(x)
        x = Conv2D(f, kernel_size=3, strides=1, padding='same')(x)
        x = BatchNormalization()(x)
        x = Activation("relu")(x)
        x = Conv2D(f, kernel_size=3, strides=1, padding='same')(x)

        if (K.int_shape(layer_input) != K.int_shape(x)):
            layer_input = Conv2D(f, kernel_size=1, strides=1, padding='same')(layer_input)

        x = Add()([layer_input, x])

        return x
    f = 16
    x = input_tensor

    for i in range(num_conv):
        x = conv_2(x, f * (i + 1))

    for i in range(num_resblock):
        x = resblock(x, 2 * f)

    #x = MaxPooling2D((2, 2))(x)
    x = Flatten()(x)
    x = Dropout(0.5)(x)
    x = Dense(256)(x)
    x = Dropout(0.5)(x)
    x = Dense(output_channels)(x)
    x = Activation("softmax")(x)

    output_tensor = x

    model = Model(inputs=input_tensor, outputs=output_tensor)

    return model


def call_build_model(p_model):
    if (p_model == 0):
        re_func = functools.partial(
            build_CNN,
            num_resblock=0,
            num_conv=2
        )
    elif (p_model == 1):
        re_func = functools.partial(
            build_CNN,
            num_resblock=2,
            num_conv=0
        )

    return re_func


def main():
    model = build_CNN(input_tensor=Input(shape=(3, 64, 2)),
                      output_channels=4,
                      num_resblock=2, num_conv=0)
    model.summary()
    pass


if __name__ == '__main__':
    main()
