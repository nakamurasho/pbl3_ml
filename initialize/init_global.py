"""

変えないグローバル変数の定義

"""

from initialize.init_result import *


# ファイル形式
mat_flag = False

# epoch
epochs = 100

# 1 ジェスチャあたりのサンプル数
gesture_per_sample = 3
# アンテナ、サブキャリア数、サンプリング
#antenna, sub_carrier, sample = 3, 30, gesture_per_sample
antenna, sub_carrier, sample = 1, 64, gesture_per_sample

# クラス数
num_gesture = 4
# ジェスチャの種類
#classes = ['choki', 'fox', 'nothing', 'paa', 'rola', 'wish']
classes = ['cho', 'goo', 'paa', 'normal']


# PBL のデータがあるフォルダ（train.py で使う）
#pub_folder = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\wifi_dataset_20190204"
#pub_folder = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_flow\demo_20190717"
#pub_folder = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_2\1PCOFDM"
pub_folder = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_0802\1PCOFDM_OS16ROFF0.8\1PCOFDM_OS16ROFF0.8"
#pub_folder = "/home/maitake/dataset/wifi_dataset_20190204"
#pub_folder = "/home/maitake/dataset/demo_20190717"

#data_dir = os.path.join(pub_folder, "data_sum")
#data_dir = pub_folder

# 交差検証用
n_kfold = 5



"""
import shutil
__setting_path = __file__
__save_path = os.path.join(result_dir, os.path.basename(__setting_path))
shutil.copy2(__setting_path, __save_path)
"""




def main():
    for i in classes:
        make_dir(os.path.join(pub_folder, i))
    pass


if __name__ == '__main__':
    main()
