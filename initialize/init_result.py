"""

結果を保存するフォルダを決める

"""


import os

# フォルダを作る
def make_dir(folder):
    if not os.path.exists(folder): os.makedirs(folder)
    return folder


# リストを結合して文字列を返す
# 引数：リスト、結合文字の中間文字列
def connect_str(attent, interpolation="_"):
    tmp_list = []
    for n, i in enumerate(attent):
        tmp_list.append(i)

        # 中間なら追加
        if (n < len(attent) - 1):
            tmp_list.append(interpolation)
        pass

    re_str = "".join(tmp_list)

    return re_str


# 結果を入れるフォルダを定義
class Result_Dir:
    # フォルダ名を決定する
    def __init__(self, root, search_p_key, search_p_dict, makedir_flag=True):
        self.makedir_flag = makedir_flag
        self.__make_dir = make_dir if self.makedir_flag else os.path.join

        self.root = self.__make_dir(root)
        self._search_p_key = search_p_key
        self._search_p_dict = search_p_dict

        # 結果を入れるフォルダ
        self.result_dir = self.__make_dir(os.path.join(self.root,
                                          self._set_result_str(self._search_p_key, self._search_p_dict)))
        self.tmp_dir = self.__make_dir(os.path.join(self.result_dir, "tmp"))

        # ファイル名を定義
        self.__setting_tmp_filename(self.tmp_dir)
        self._setting_file_name(self.result_dir)
        pass

    # パラメータをつなげたフォルダ名を作成
    def _set_result_str(self, search_p_key, search_p_dict):
        tmp = ["{}"] * len(search_p_key)
        tmp = connect_str(attent=tmp, interpolation="_")

        # キーに対応する値を取り出す
        value = []
        for key in search_p_key:
            value.append(search_p_dict[key])
            pass

        re_result = tmp.format(*value)

        return re_result

    # 交差検証での中間ファイル（使わない）
    def __setting_tmp_filename(self, path):
        # 学習結果を保存するファイル名（format: fold）
        self.tmp_history = os.path.join(path, "history_{}.txt")
        # 学習曲線（format: fold）
        self.tmp_accuracy = os.path.join(path, 'accuracy_{}.pdf')
        self.tmp_loss = os.path.join(path, 'loss_{}.pdf')

        # 混合行列を保存する名前（format: fold）
        self.tmp_cm_train = os.path.join(path, "cm_train_{}.pdf")
        self.tmp_cm_test = os.path.join(path, "cm_test_{}.pdf")
        pass

    def _setting_file_name(self, path):
        # 学習結果を保存するファイル名（format: "average"）
        self.history_file_name = os.path.join(path, "history_{}.txt")
        #self.history_file_name = os.path.join(path, "history.txt")

        # 学習曲線（format: "average"）
        self.accuracy_file_name = os.path.join(path, 'accuracy_pdf.png')
        self.loss_file_name = os.path.join(path, 'loss_pdf.png')

        # 混合行列を保存する名前（format: "average"）
        self.cm_train_name = os.path.join(path, "cm_train_pdf.png")
        self.cm_test_name = os.path.join(path, "cm_test_pdf.png")

        # summary ファイル
        self.summary_file_name = os.path.join(path, "model.txt")

        # model 保存
        self.save_model_name = os.path.join(path, "model.h5")

        pass
    pass



def main():
    pass


if __name__ == '__main__':
    main()