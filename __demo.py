"""

pbl3 のデモ

1. 超音波の波形をもらう（読み込み）
2. 波形を変換（前処理）
3. 予測
4. 結果をもとに if 文か何かでデモ

"""

from keras.layers import Input
import cv2
import numpy as np

import build_model
import load_mat
import load_dataset

from initialize.init_global import *


# 学習済みの重みファイル
generator_weight_name = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\result\result\test_20190522_sample3_res_conv2\weight\gesture_weights.h5"

# ウィンドウサイズ
_size = 1.25
size_w = int(1920 / _size)
size_h = int(1080 / _size)


concat_axis = 3
# 音波データファイル（mat file）
file_name = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\wifi_dataset_20190204\train\choki\20190204_choki.mat"
# CNN に使う
num_resblock, num_conv = 0, 2


class PBL3_Demo:
    def __init__(self):
        # モデルを設定
        self.model = self.init_get_model()

        # ジェスチャ名を数値に変換する辞書
        # TODO: 使わないかも
        self.classes2id = self.init_re_classes2id()

        # 各ジェスチャに対応する結果の辞書を作成する
        self.id2gesture = self.init_id2gesture()

        # デモ開始準備
        self.init_assert()

        # デモ
        self.demo_main()
        pass
    def init_get_model(self):
        input_shape = (sample, 2 * sub_carrier, antenna)
        if (concat_axis == 3): input_shape = (sample, sub_carrier, 2 * antenna)
        # ネットワークの入力
        input_tensor = Input(shape=input_shape)

        # モデルを構築
        model = build_model.build_CNN(input_tensor=input_tensor, output_channels=num_gesture,
                                      num_resblock=num_resblock, num_conv=num_conv)

        # 重みを読み込む
        model.load_weights(generator_weight_name)

        return model
    # ジェスチャ名を数値に変換する辞書（使わないかも）
    def init_re_classes2id(self):
        classes2id = {}
        # gesture (classes) をキーにした辞書を作成
        for i in classes: classes2id[i] = classes.index(i)

        return classes2id
    # 各ジェスチャに対応する結果の辞書を作成する
    def init_id2gesture(self):
        id2gesture = {}

        img_dir = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\gesture_result"
        # 画像名の辞書
        img_name_dict = {
            'choki': "choki.png",
            'fox': "test.png",
            'nothing': "nothing.png",
            'paa': "paa.png",
            'rola': "choki.png",
            'wish': "wish.png"
        }

        for i in classes:
            img_name = img_name_dict[i]
            img_name = os.path.join(img_dir, img_name)
            # 画像読み込み
            img = cv2.imread(img_name)

            # 画像のリサイズ
            img = cv2.resize(img, (size_w, size_h))

            id2gesture[classes.index(i)] = img
            pass

        return id2gesture
    # デモ開始準備
    def init_assert(self):
        # 使用するジェスチャ名を表示
        print(classes)
        print("num gesture: {}".format(len(classes)))

        # 各ジェスチャに対応する結果の辞書が設定されているか確認
        for i in classes:
            gesture = self.id2gesture[classes.index(i)]
            assert gesture is not None, i
            #print(gesture)

        return

    # 超音波のデータを読み込む
    def __get_supersound_data(self):
        # TODO: 超音波データの取り扱いをどうするか話し合う
        # TODO: mat ファイルの場合ファイル名をどうするか考える
        #file_name = ""
        # mat ファイルを読み込む
        mat_data = load_mat.load_mat_file(file_name)

        re_data = mat_data

        return re_data
    # 読み込んだデータの前処理
    def __preprocess_data(self, data):
        # TODO: mat ファイルの場合
        mat_data = data
        # データを取り出す（データを表すキー）
        data = load_mat.get_mat_value(mat_data, "csimat")

        # TODO: データ数がバラバラの場合整形する（数 sample だけ取り出す）
        data = data[:gesture_per_sample, :, :]

        # テンソルを作成
        data = np.expand_dims(data, axis=0)

        # 振幅と位相に分けて虚数をコンカチ
        data = load_dataset.data_processing(data, concat_axis=concat_axis)

        return data
    # データをモデルに渡すまでの処理
    def re_supersound_data(self):
        # 超音波データを読み込む
        re_data = self.__get_supersound_data()

        # データに対する前処理
        re_data = self.__preprocess_data(re_data)

        return re_data
    # デモ
    def demo(self, pred):
        # ['choki', 'fox', 'nothing', 'paa', 'rola', 'wish']
        """ それぞれのジェスチャの確率を表示 """
        tub_list = ["\t", "\t\t", "\t", "\t\t", "\t", "\t"]
        # 確率を取り出す
        prob = pred[0]
        prob *= 100
        prob = prob.tolist()
        # 文字列を作成
        tmp_text = []
        for c, p, t in zip(classes, prob, tub_list):
            tmp_text.append("{}{}：{}\n".format(c, t, p))
            pass
        text = "".join(tmp_text)
        print(text)

        """
        print(pred)
        for i in pred:
            pass
        # 画像を取り出す
        pred = np.argmax(pred[0])
        pred = 1
        # 各ジェスチャに割り当てられた画像を表示する
        img = self.id2gesture[pred]

        # 文字を描画
        # 表記位置
        #pos = (10, size_w - 10)
        pos = (10, 10)
        # 大きさ
        fontScale = 1
        # 太さ
        thickness = 2
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img=img, text=text,
                    org=pos, fontFace=font,
                    fontScale=fontScale, color=(255, 255, 0),
                    thickness=thickness, lineType=cv2.LINE_AA)
        window_name = "demo"
        cv2.imshow(window_name, img)
        cv2.waitKey(1)
        cv2.destroyAllWindows()
        """

        return
    # デモまとめ
    def demo_main(self):
        while(True):
            # データを読み込む
            data = self.re_supersound_data()

            # モデルで予測
            pred = self.model.predict(data)

            # 予測結果を元に何かする
            self.demo(pred)
            pass
        return
    pass


def main():
    PBL3_Demo()
    pass


if __name__ == '__main__':
    main()