"""

グリッドサーチしたパラメータから最適を探す

"""

from pprint import pprint

from initialize.init_global import *
from _init_setting import *

import fit_visualize
import argparse



# acc, loss を並べてみる
def set_acc_2(root, top=5):
    result_dir_list = []
    search_p_dict_list = []

    # root の中身を見ていく
    param_list = os.listdir(root)

    for p in param_list:
        p = p.split("_")
        search_p_dict = dict(zip(search_p_list, p))
        result_dir = Result_Dir(root=root,
                                search_p_key=search_p_list,
                                search_p_dict=search_p_dict,
                                makedir_flag=False)
        search_p_dict_list.append(search_p_dict)
        result_dir_list.append(result_dir)
        pass

    # 値を取り出してリストに追加していく
    val_acc_list = []
    val_loss_list = []

    for result_dir in result_dir_list:
        history_file_name = result_dir.history_file_name

        # history がなければ次へ
        if (not(os.path.isfile(history_file_name))): continue

        # val_acc の一番高い値を出す
        epoch, loss, acc, val_loss, val_acc = fit_visualize.load_result_txt(history_file_name)
        val_acc = max(val_acc)

        # リストに追加
        val_acc_list.append(val_acc)
        val_loss_list.append(val_loss)
        pass

    # val_acc の高い順に並べる
    c = zip(val_acc_list, val_loss_list)
    c = sorted(c, reverse=True)
    tmp_acc_list, tmp_loss_list = zip(*c)

    p_list = []

    top = len(tmp_acc_list) if (top is None) else top
    # 高い順にファイルを取り出す
    for i in range(top):
        # ソートされた値を元にインデックスを取り出す
        index = val_acc_list.index(tmp_acc_list[i])

        result_dir = result_dir_list[index]
        search_p_dict = search_p_dict_list[index]
        p_list.append(search_p_dict)
        pprint(search_p_dict)
        pass

    return



def main():
    # root = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\result_grid\test"
    root = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_flow\result\test_4\test_4"
    root = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_2\result_1PCOFDM\test_20190721"
    root = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_0802\20190802_2"
    top = None
    top = 1

    # ArgumentParserの使い方を簡単にまとめた
    # https://qiita.com/kzkadc/items/e4fc7bc9c003de1eb6d0
    # parser を作る
    parser = argparse.ArgumentParser()
    # 3. parser.add_argumentで受け取る引数を追加していく
    parser.add_argument("--root", type=str, default=root, help="save")
    parser.add_argument("--top", type=int, default=top, help="data")
    # 4. 引数を解析
    args = parser.parse_args()

    set_acc_2(args.root, args.top)
    pass


if __name__ == '__main__':
    main()