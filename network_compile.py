"""
ネットワークコンパイル
"""

from keras.layers import Input, Dense, Reshape, Flatten, Dropout, Add
from keras.layers import BatchNormalization, Activation, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras.models import Sequential, Model
from keras.optimizers import Adam, SGD
from keras.utils import np_utils
from keras.layers import Concatenate
from keras.layers import Lambda
from keras.layers import MaxPooling2D, AveragePooling2D
from keras import initializers


def compile_signfi(model, opt, lr):
    if (opt == "Adam"):
        optimizer = Adam(lr=lr, beta_1=0.9, beta_2=0.999)
    elif (opt == "SGD"):
        optimizer = SGD(lr=lr, momentum=0.9)

    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])

    return model


def print_summary(model, summary_file):
    model.summary()

    from io import StringIO
    with StringIO() as buf:
        # StringIOに書き込む
        model.summary(print_fn=lambda x: buf.write(x + "\n"))
        # StringIOから取得
        text = buf.getvalue()
    with open(summary_file, mode="w") as f:
        f.write(text)

    return


def main():
    pass


if __name__ == '__main__':
    main()
