"""
history を可視化する
"""

import matplotlib.pyplot as plt


# 結果をテキストに保存
# 引数：history、保存するテキストファイル名
def save_history_txt(history, save_file_name):
    loss = history.history['loss']
    acc = history.history['acc']
    val_loss = history.history['val_loss']
    val_acc = history.history['val_acc']
    nb_epoch = len(acc)

    with open(save_file_name, "w") as fp:
        fp.write("epoch\tloss\tacc\tval_loss\tval_acc\n")
        for i in range(nb_epoch):
            fp.write(u"%d\t%f\t%f\t%f\t%f\n" % (i, loss[i], acc[i], val_loss[i], val_acc[i]))

    return


# テキストファイルからデータを読み込む
# 引数：ファイル名
# 戻り値：エポック、ロス、精度、バリデーションデータのロス、精度
def load_result_txt(file_name):
    epoch_list = []
    loss_list = []
    acc_list = []
    val_loss_list = []
    val_acc_list = []

    with open(file_name) as fp:
        fp.readline()  # skip title
        for line in fp:
            line = line.rstrip()
            cols = line.split('\t')

            assert len(cols) == 5

            epoch = int(cols[0])
            loss = float(cols[1])
            acc = float(cols[2])
            val_loss = float(cols[3])
            val_acc = float(cols[4])

            epoch_list.append(epoch)
            loss_list.append(loss)
            acc_list.append(acc)
            val_loss_list.append(val_loss)
            val_acc_list.append(val_acc)

    return epoch_list, loss_list, acc_list, val_loss_list, val_acc_list



# 学習曲線に描く
def make_learning_curve(history_file_name, save_acc_name, save_loss_name, acc_limit=(0.0, 1.02), loss_limit=None):
    # 結果を読み込む
    epoch, loss, acc, val_loss, val_acc = load_result_txt(history_file_name)

    # accuracy
    plt.figure()
    plt.plot(epoch, acc, 'r-', marker='.', label='train_acc')
    plt.plot(epoch, val_acc, 'b-', marker='.', label='val_acc')
    # グリッド線
    plt.grid()
    # 凡例
    plt.legend()
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    # 精度の幅
    #plt.ylim([0.0, 1.02])
    plt.ylim(acc_limit)
    # 保存
    plt.savefig(save_acc_name)
    plt.close()

    # loss
    plt.figure()
    plt.plot(epoch, loss, 'r-', marker='.', label='train_loss')
    plt.plot(epoch, val_loss, 'b-', marker='.', label='val_loss')
    plt.grid()
    plt.legend()
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.ylim(loss_limit)
    #plt.ylim([loss_limit[0], [loss_limit[1]]])
    plt.savefig(save_loss_name)
    plt.close()

    return


def main():
    pass


if __name__ == '__main__':
    main()
