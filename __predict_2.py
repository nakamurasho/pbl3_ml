"""

学習したモデルで入力 CSI -> 出力グーチョキパー判定
関数作成

"""


from keras.models import load_model

import numpy as np
import pickle

import load_dataset



# debug 用
def __load_pickle(file_name):
    # サブキャリア数サイズの np.array がサンプル数分入ったリスト
    # [(64, )... (64, )]
    with open(file_name, 'rb') as f:
        data = pickle.load(f, encoding='latin1')

    sub_carrier = 64

    # 20190720 追加（なぜかリスト内の要素が欠けてた）
    for l in data:
        s = len(l)
        add_len = sub_carrier - s
        if (add_len > 0):
            add_data = [0j] * add_len
            l.extend(add_data)
        elif (add_len < 0):
            del l[sub_carrier:]
        pass

    # (サンプル数、サブキャリア数)
    data = np.array(data)

    # csi の型は (64, ) を 3 つ合わせて (3, 64) の numpy.array にする
    data = data[:3, :]

    return data


# 引数：model、csi データ（(3, 64) の numpy.array）
def csi2gesture(model, csi):
    # 前処理
    data = np.array(csi)
    # チャネル方向に次元を増やす
    data = data[np.newaxis, :, :, np.newaxis]
    # 振幅と位相に分けて連結
    # 軸は前もって決める
    data = load_dataset.data_processing(data, concat_axis=3)

    # 予測
    pred = model.predict(data)

    # ラベルに変更
    pred, = np.argmax(pred, axis=1)

    return pred


def __usage(model_file_name):
    # model 構築
    model = load_model(model_file_name)

    # csi データを用意
    #csi = __load_pickle(r"1PCOFDM\cho\cho_chan_coeffs_pickle")
    csi = __load_pickle(r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_2\1PCOFDM\cho\cho_chan_coeffs_pickle")

    # csi の型は (64, ) を 3 つ合わせて (3, 64) の numpy.array にする
    # (サンプル数：3、サブキャリア数：64)
    label = csi2gesture(model, csi)

    # ラベルと対応したジェスチャ名
    label2gesture = ['cho', 'goo', 'paa', 'normal']
    gesture = label2gesture[label]

    print(gesture)

    return



def main():
    # モデルのパス
    #model_file_name = r"result_1PCOFDM\save_test_20190721\1_64_0.001_Adam_3\model.h5"
    #model_file_name = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_2\result_1PCOFDM\save_test_20190721\1_64_0.001_Adam_3\model.h5"
    model_file_name = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_0802\20190802\1_256_0.0001_Adam_3\model.h5"
    __usage(model_file_name)
    return


if __name__ == '__main__':
    main()
    pass