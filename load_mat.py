import scipy.io
import numpy as np


# mat ファイルを読み込む
# 引数：ファイル名
# 戻り値：mat データ（辞書型）
def load_mat_file(mat_file_name):
    #print("loading")
    mat_data = scipy.io.loadmat(mat_file_name)
    #mat_data = np.loadtxt(mat_file_name)
    #mat_data = np.genfromtxt(mat_file_name)

    return mat_data


# mat ファイルに書き込む（テスト用）
def write_mat_file(save_file_name, key, value):
    # mat ファイルを出力するときは辞書で渡す
    #scipy.io.savemat(r"C:\Users\nakamura sho\Documents\user\PBL2\20190107\data-1.mat", {"csid_lab": data})
    scipy.io.savemat(save_file_name, {key: value})

    return


# mat ファイルの中身を取り出す
# 引数：mat データ（辞書型）、キー
# 戻り値：対応する値
def get_mat_value(mat_data, key):
    value = mat_data[key]

    return value




def main():
    pass


if __name__ == '__main__':
    main()
