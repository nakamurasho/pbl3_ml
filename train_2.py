"""

学習
・交差検証で評価
・グリッドサーチでパラメータを決定

・loss の最小値を取り出して平均をとる
→ 重みは再学習時に取得する
・各パラメータの評価値がすぐ見れるようにする
・


"""

from keras.layers import Input, Dense, Reshape, Flatten, Dropout, Add
from keras.layers import BatchNormalization, Activation, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.utils import np_utils
from keras.layers import Concatenate
from keras.layers import Lambda
from keras.layers import MaxPooling2D, AveragePooling2D
from keras import initializers
from keras.callbacks import LearningRateScheduler

# 混合行列を求められる
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold

import network_compile
import build_model
import fit_visualize
import load_dataset
import calculate_result

from initialize.init_global import *
from _init_setting import *
import numpy as np
import argparse



# 交差検証
# 引数：モデル構築関数、
def train_kfold(data_dir, model_func, concat_axis, optimizer, lr, batch_size, Result_Dir):
    def step_decay(epoch):
        x = lr
        if epoch >= 50: x /= 10
        return x

    # 入力形式(200, 60, 3) or (20, 30, 6)
    input_shape = (sample, 2 * sub_carrier, antenna)
    if (concat_axis == 3): input_shape = (sample, sub_carrier, 2 * antenna)

    # データとラベルを取り出す
    data, label = load_dataset.re_dataset(data_dir, concat_axis=concat_axis)

    ckbk = []
    lr_decay = LearningRateScheduler(step_decay)
    ckbk.append(lr_decay)

    # k 交差検証
    kfold = StratifiedKFold(n_splits=n_kfold, shuffle=True, random_state=1)

    history_dict = {}
    cm_l_t, cm_p_t = [], []
    cm_l_v, cm_p_v = [], []

    for train, test in kfold.split(data, label):
        # モデルを構築
        model = model_func(input_tensor=Input(shape=input_shape), output_channels=num_gesture)
        # モデルを書き出す
        network_compile.print_summary(model, Result_Dir.summary_file_name)
        # ネットワークコンパイル
        model = network_compile.compile_signfi(model, optimizer, lr)

        y = np_utils.to_categorical(label[train], num_classes=len(classes))
        val_y = np_utils.to_categorical(label[test], num_classes=len(classes))

        """ 学習 """
        history = model.fit(
            x=data[train], y=y,
            batch_size=batch_size, epochs=epochs,
            verbose=1, callbacks=ckbk,
            validation_data=(data[test], val_y)
        )
        # 学習データで混合行列を作る
        _label, _pred = calculate_result.re_preparation_cm(model, data=data[train], label=y)
        cm_l_t.append(_label)
        cm_p_t.append(_pred)
        # テストデータで混合行列を作る
        _label, _pred = calculate_result.re_preparation_cm(model, data=data[test], label=val_y)
        cm_l_v.append(_label)
        cm_p_v.append(_pred)

        # k 個分追加
        for i in history.history.keys():
            # 要素を取得
            value = history.history[i]
            # 辞書にキーと値を追加
            history_dict.setdefault(i, np.zeros(shape=len(value)))
            # 辞書に足していく
            history_dict[i] = np.add(history_dict[i], value)
        pass
    # 最後に平均をとる
    for i in history_dict.keys():
        # 辞書内に合計が入っている
        history_dict[i] /= n_kfold
        pass
    history.history = history_dict

    # 混合行列を作成
    _label = np.concatenate(cm_l_t, axis=0)
    _pred = np.concatenate(cm_p_t, axis=0)
    calculate_result.make_confusion_matrix(
        save_name=Result_Dir.cm_train_name, label=_label, pred=_pred, classes=classes
    )

    # 検証データで混合行列を作成
    _label = np.concatenate(cm_l_v, axis=0)
    _pred = np.concatenate(cm_p_v, axis=0)
    calculate_result.make_confusion_matrix(
        save_name=Result_Dir.cm_test_name, label=_label, pred=_pred, classes=classes
    )

    """ 学習後の処理 """
    # history をファイルに保存
    fit_visualize.save_history_txt(history, Result_Dir.history_file_name)

    # history 可視化
    fit_visualize.make_learning_curve(Result_Dir.history_file_name,
                                      Result_Dir.accuracy_file_name,
                                      Result_Dir.loss_file_name)

    return



def train_grid_search(data_dir, root):
    for model in p_architecture:
        # モデルを定義
        model_func = build_model.call_build_model(p_model=model)
        # コンカチする軸
        for concat_axis in p_concat_axis:

            # オプティマイザ
            for optimizer in p_optimizer:
                # 学習率
                for lr in p_lr:
                    # バッチサイズ
                    for batch_size in p_batch_size:
                        search_p_dict = {"model": model,
                                         "batch_size": batch_size,
                                         "lr": lr,
                                         "optimizer": optimizer,
                                         "concat_axis": concat_axis}
                        # 結果を入れるフォルダ
                        result_dir = Result_Dir(root=root,
                                                search_p_key=search_p_list,
                                                search_p_dict=search_p_dict,
                                                makedir_flag=True)
                        # 学習
                        train_kfold(data_dir, model_func, concat_axis, optimizer, lr, batch_size, result_dir)
                        pass
        pass

    return




def main():
    # ArgumentParserの使い方を簡単にまとめた
    # https://qiita.com/kzkadc/items/e4fc7bc9c003de1eb6d0
    # parser を作る
    parser = argparse.ArgumentParser()
    # 3. parser.add_argumentで受け取る引数を追加していく
    parser.add_argument("--data", type=str, default=pub_folder, help="data")
    parser.add_argument("--root", type=str, default=ex_word, help="save")
    # 4. 引数を解析
    args = parser.parse_args()
    # サンプル
    #args.data = "1PCOFDM_OS16ROFF0.8"

    train_grid_search(data_dir=args.data, root=args.root)

    return


if __name__ == '__main__':
    main()
    pass

