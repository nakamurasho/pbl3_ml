## 使い方

## 環境
python3.6
tensorflow1.9.0
keras2.2.4

## Wiki
https://gitlab.com/stalagmite/pblacoustic/wikis/nakamura

## 学習
`python3 train_2.py --data path --root path_2`

学習するデータがあるフォルダのパスを引数の `path` に指定してください。
結果の保存先を引数の `path_2` に指定してください。
データはジェスチャごとにフォルダ分けしてください。
2080Ti で 1 ～ 3 時間かかります（測ってないので曖昧です）。

```
path
├── cho
|   ├── cho.pickle
|   └── ...
|
|── paa
|   ├── paa.pickle
|   └── ...
|
|── goo
└── normal
 ```

### 説明
グリッドサーチで最適なパラメータを探します。
変更可能なパラメータは、
1. CNNのアーキテクチャ
2. バッチサイズ
3. 学習率
4. 最適化アルゴリズム

などがあり、`_init_setting.py` にリストで設定されています。
リスト内のすべての組み合わせを計算します。


## 学習後
`python3 evaluation_2.py --root path_2`

学習が終わったら最適なパラメータを調べます。
学習結果が保存されているフォルダを引数の `path_2` に指定してください。
出力にパラメータが出ます。


## パラメータ取得
`python3 train_savemodel.py --data path --batch_size param_1 --lr param_2 --root path_3`

最適なパラメータを見つけたらそのパラメータを保存します。
学習後に出てきたパラメータの中から batch_size と lr を探して、引数の `param_1` と `param_2` に置き換えてください。
パラメータの保存先は引数の `path_3` に指定してください。


## 他のパラメータ
epoch, subcarrir, classes（ジェスチャのラベル）などは `initialize/init_global.py` に書いてあるので変更してください。

## サンプルデータ
`1PCOFDM_OS16ROFF0.8` フォルダにサンプルデータがあります。


## モデル構築
`build_model.py` に書いてあります。


## データ読み込み
`load_dataset.py` に書いてあります。
mat ファイルを使うときは `load_mat.py` を参考にしてください。


## 予測
`predict.py` 内の `csi2gesture()` 関数を参考にしてください。




