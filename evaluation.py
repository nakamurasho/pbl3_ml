"""

グリッドサーチしたパラメータから最適を探す

"""

from pprint import pprint
import sys

from initialize.init_global import *
from _init_setting import *

import fit_visualize
import viewer
from PyQt5 import QtWidgets
import pdf2image
import numpy as np
import cv2


def load_pdf2image():
    # https://github.com/Belval/pdf2image
    # https://shotanuki.com/python%E3%81%A7pdf%E3%82%92%E7%94%BB%E5%83%8F%E3%81%AB%E5%A4%89%E6%8F%9B%E3%81%99%E3%82%8B/
    # 【Windows】【Python】PDFファイルを画像化してOpenCVで扱う
    # https://qiita.com/Kazuhito/items/8a626c12278a21e75a20
    # popplerへの環境変数PATHを一時的に付与
    path = r"C:\Users\nakamura sho\PycharmProjects\pbl3_ml\poppler-0.68.0_x86"
    poppler_path = os.path.join(path, "poppler-0.68.0", "bin")
    os.environ["PATH"] += os.pathsep + poppler_path

    return

def load_pdf(pdf_list):
    re_img_list = []
    for i in pdf_list:
        img = pdf2image.convert_from_path(i)
        img = np.asarray(img[0])
        re_img_list.append(img)
        pass

    return re_img_list

def load_img(img_list):
    re_img_list = []
    for i in img_list:
        img = cv2.imread(i)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, (1280, 960))
        re_img_list.append(img)
        pass

    return re_img_list


# acc, loss を並べてみる
def set_acc(root, top=5):
    val_acc_list = []
    result_dir_list = []
    search_p_dict_list = []

    for model in p_architecture:
        # コンカチする軸
        for concat_axis in p_concat_axis:
            # オプティマイザ
            for optimizer in p_optimizer:
                # 学習率
                for lr in p_lr:
                    # バッチサイズ
                    for batch_size in p_batch_size:
                        search_p_dict = {"model": model,
                                         "batch_size": batch_size,
                                         "lr": lr,
                                         "optimizer": optimizer,
                                         "concat_axis": concat_axis}
                        # 結果を入れるフォルダ

                        result_dir = Result_Dir(root=root,
                                                search_p_key=search_p_list,
                                                search_p_dict=search_p_dict,
                                                makedir_flag=False)
                        search_p_dict_list.append(search_p_dict)
                        result_dir_list.append(result_dir)
                        pass
        pass

    for result_dir in result_dir_list:
        # history
        history_file_name = result_dir.history_file_name

        if (not(os.path.isfile(history_file_name))): continue

        # val_acc の一番高い値を出す
        epoch, loss, acc, val_loss, val_acc = fit_visualize.load_result_txt(history_file_name)
        val_acc = max(val_acc)
        val_acc_list.append(val_acc)
        pass

    # 高い順に並べる
    tmp_list = sorted(val_acc_list, reverse=True)

    acc_list = []
    p_list = []

    top = len(tmp_list) if (top is None) else top

    for i in range(top):
        index = val_acc_list.index(tmp_list[i])

        result_dir = result_dir_list[index]
        search_p_dict = search_p_dict_list[index]
        p_list.append(search_p_dict)
        pprint(search_p_dict)

        # accファイルを取り出す
        acc = result_dir.accuracy_file_name
        acc_list.append(acc)
        pass

    load_pdf2image()
    acc_pdf_list = load_pdf(acc_list)

    QApp = QtWidgets.QApplication(sys.argv)
    app = viewer.Application(acc_list, acc_pdf_list)
    app.show()
    sys.exit(QApp.exec_())


    return


# acc, loss を並べてみる
def set_acc_2(root, top=5):
    result_dir_list = []
    search_p_dict_list = []

    # root の中身を見ていく
    param_list = os.listdir(root)

    for p in param_list:
        p = p.split("_")
        search_p_dict = dict(zip(search_p_list, p))
        result_dir = Result_Dir(root=root,
                                search_p_key=search_p_list,
                                search_p_dict=search_p_dict,
                                makedir_flag=False)
        search_p_dict_list.append(search_p_dict)
        result_dir_list.append(result_dir)
        pass

    # 値を取り出してリストに追加していく
    val_acc_list = []
    val_loss_list = []

    for result_dir in result_dir_list:
        history_file_name = result_dir.history_file_name

        # history がなければ次へ
        if (not(os.path.isfile(history_file_name))): continue

        # val_acc の一番高い値を出す
        epoch, loss, acc, val_loss, val_acc = fit_visualize.load_result_txt(history_file_name)
        val_acc = max(val_acc)

        # リストに追加
        val_acc_list.append(val_acc)
        val_loss_list.append(val_loss)
        pass

    # val_acc の高い順に並べる
    c = zip(val_acc_list, val_loss_list)
    c = sorted(c, reverse=True)
    tmp_acc_list, tmp_loss_list = zip(*c)

    acc_img_list = []
    loss_img_list = []
    train_cm_list = []
    test_cm_list = []
    p_list = []

    top = len(tmp_acc_list) if (top is None) else top
    # 高い順にファイルを取り出す
    for i in range(top):
        # ソートされた値を元にインデックスを取り出す
        index = val_acc_list.index(tmp_acc_list[i])

        result_dir = result_dir_list[index]
        search_p_dict = search_p_dict_list[index]
        p_list.append(search_p_dict)
        pprint(search_p_dict)

        # accファイルを取り出す
        acc = result_dir.accuracy_file_name
        acc_img_list.append(acc)
        loss = result_dir.loss_file_name
        loss_img_list.append(loss)
        # cm を取り出す
        train_cm_list.append(result_dir.cm_train_name)
        test_cm_list.append(result_dir.cm_test_name)
        pass

    # val_acc.pdf を画像に変換
    load_pdf2image()
    acc_pdf_list = load_pdf(acc_img_list)
    # TODO: 間違えて loss は png で保存した
    loss_pdf_list = load_img(loss_img_list)
    train_cm_list = load_img(train_cm_list)
    test_cm_list = load_img(test_cm_list)

    # 画像作成
    figure_list = []
    for i in range(len(acc_pdf_list)):
        acc = acc_pdf_list[i]
        loss = loss_pdf_list[i]
        tmp = np.concatenate([acc, loss], axis=1)

        train_cm = train_cm_list[i]
        test_cm = test_cm_list[i]
        tmp_cm = np.concatenate([train_cm, test_cm], axis=1)

        figure = np.concatenate([tmp, tmp_cm], axis=0)
        figure_list.append(figure)
        pass

    QApp = QtWidgets.QApplication(sys.argv)
    # 画像名のリスト、画像のリスト
    app = viewer.Application_2(acc_img_list, figure_list)
    app.show()
    sys.exit(QApp.exec_())

    return



def main():
    #root = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\result_grid\test"
    root = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_flow\result\test_4\test_4"
    root = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_2\result_1PCOFDM\test_20190721"
    root = r"C:\Users\nakamura sho\Documents\user\PBL\PBL3\demo_0802\20190802_2"
    top = None
    #top = 3
    set_acc_2(root, top)
    pass


if __name__ == '__main__':
    main()