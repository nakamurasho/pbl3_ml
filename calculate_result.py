"""
学習結果の混合行列や精度を計算
"""

import numpy as np
import matplotlib.pyplot as plt
import itertools
from sklearn.metrics import confusion_matrix


# 引数：混合行列、クラス数、出力ファイル名、正規化、タイトル、色
def plot_confusion_matrix(cm, classes, output_file,
                          normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):

    # 正規化
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.savefig(output_file, bbox_inches="tight")
    plt.close()

    return


# 混合行列を作成する
# 引数：モデル、データ、正解ラベル
def make_confusion_matrix(save_name, label, pred, classes):
    # confusion matrix を出力
    cm = confusion_matrix(y_true=label, y_pred=pred)

    # ファイルに保存
    plot_confusion_matrix(cm, classes=classes, output_file=save_name)

    return


# 混合行列を作成するために予測とラベルを返す
def re_preparation_cm(model, data, label):
    # 予測を出す
    pred = model.predict(data)

    # one-hot-vector を数値ラベルに戻す
    pred = np.argmax(pred, axis=1)
    label = np.argmax(label, axis=1)

    return label, pred

# 混合行列を作成するための予測とラベルを追加していく
def add_pred_cm(label, pred, label2, pred2):
    re_label = np.concatenate([label, label2], axis=0)
    re_pred = np.concatenate([pred, pred2], axis=0)

    return re_label, re_pred


def main():
    from keras.utils import np_utils
    label = np.random.randint(0, 5, 30)
    label = np_utils.to_categorical(label, 5)
    print(label.shape)
    # one-hot-vector を数値ラベルに戻す
    pred = np.argmax(label, axis=1)
    label = np.argmax(label, axis=1)
    print(label.shape)

    # confusion matrix を出力
    # label = ["tyoki"]
    cm = confusion_matrix(y_true=label, y_pred=pred)  # , label=)

    print(cm)

    plot_confusion_matrix(cm, classes=range(5), output_file="a.pdf")
    pass


if __name__ == '__main__':
    main()
