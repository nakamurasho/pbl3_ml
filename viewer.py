"""

画像ビューアー
・PyQt5にmatplotlibを埋め込んで簡易画像ビューアを作る
https://qiita.com/ceptree/items/c3a7c52cdd152c9f62d9

"""

import sys
import os
import numpy as np
import matplotlib.pyplot as plt

from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

from PIL import Image


# ウィンドウ位置
x, y = 100, 100

# ウィンドウサイズ
window_x, window_y = 1500, 900

# ファイルウィンドウサイズ
flist_x, flist_y = 200, window_y


class Application(QtWidgets.QWidget):
    def __init__(self, img_name_list, img_list=None):
        super().__init__()
        # 画像名のリスト
        self.img_name_list = img_name_list
        # 表示する画像名のリスト
        self.Files = [os.path.basename(os.path.dirname(f)) for f in self.img_name_list]

        # 画像を読み込む
        self.img_list = self.load_imgs(self.img_name_list) if (img_list is None) else img_list

        # UIの初期化
        self.initUI()

        # ファイルを配置
        self.set_FileList()

        # 画像
        self.FileName = self.FileList.item(0).text()
        self.idx = 0

        # 画像を読み込む
        self.load_ImageFile()

        # Figureの初期化
        self.initFigure()

        # ファイルをクリックしたとき
        self.FileList.currentRowChanged.connect(self.listMove)

        # ファイルを変更した時のイベント
        #self.FileList.itemSelectionChanged.connect(self.FileList_Changed)
        pass
    def listMove(self, item):
        #selected_idx = self.FileList.currentRow()
        # 画像を取り出す
        img = self.img_list[item]

        # Figure を更新
        self.update_Figure(img)
        #item.setBackground(QtGui.QColor('red')) #クリックされた行の色変更
        #print(a) #こっちは逆にitemから行番号が取得できないからこうした
        return

    # UIの初期化
    def initUI(self):
        # Figure用のWidget
        self.FigureWidget = QtWidgets.QWidget(self)
        # FigureWidgetにLayoutを追加
        self.FigureLayout = QtWidgets.QVBoxLayout(self.FigureWidget)
        # Marginを消す
        self.FigureLayout.setContentsMargins(0, 0, 0, 0)

        # ファイルのリスト
        self.FileList = QtWidgets.QListWidget(self)

        # 配置
        # 一番外枠のウィンドウ位置
        self.setGeometry(x, y, 900, 600)
        # FigureWidget のウィンドウ位置（ウィンドウサイズ 500×600）
        self.FigureWidget.setGeometry(200, 0, 700, 600)
        self.FileList.setGeometry(0, 0, 200, 600)
        pass

    # Figureの初期化
    def initFigure(self):
        # Figureを作成
        self.Figure = plt.figure()
        # FigureをFigureCanvasに追加
        self.FigureCanvas = FigureCanvas(self.Figure)
        # LayoutにFigureCanvasを追加
        self.FigureLayout.addWidget(self.FigureCanvas)

        self.axis = self.Figure.add_subplot(1, 1, 1)
        self.axis_img = self.axis.imshow(self.image, cmap='gray')
        plt.axis('off')
        pass

    # ファイルを配置
    def set_FileList(self):
        # ファイルリストに追加
        for img_name in self.Files:
            self.FileList.addItem(img_name)
        pass

    # 画像ファイルを読み込む
    def load_imgs(self, img_name_list):
        re_img_list = []

        for i in img_name_list:
            img = Image.open(i)
            img = np.asarray(img)
            re_img_list.append(img)
            pass
        return re_img_list

    # 画像ファイルを読み込む
    def load_ImageFile(self):
        # 画像を開く
        image = self.img_list[self.idx]
        # numpy.ndarrayに
        self.image = np.asarray(image)

    # ファイルを変更した時の関数
    def FileList_Changed(self):
        # 選択された
        selected_idx = self.FileList.SelectedClicked
        # 選択しているファイルの名前を取得
        a = self.FileList
        b = self.FileList.selectedItems()
        c = self.FileList.selectedItems()[0]
        d = self.FileList.selectedItems()[0].text()

        selected_name = self.FileList.selectedItems()[0].text()

        # ファイル名のインデックスを取得
        self.idx = self.show_fname_list.index(selected_name)

        # 画像を取り出す
        img = self.img_list[self.idx]

        # Figure を更新
        self.update_Figure(img)
        pass

    # Figureを更新
    def update_Figure(self, img):
        self.axis_img.set_data(img)
        self.FigureCanvas.draw()
        pass



class Application_2(QtWidgets.QWidget):
    def __init__(self, img_name_list, img_list=None):
        super().__init__()
        # 画像名のリスト
        self.img_name_list = img_name_list
        # 表示する画像名のリスト
        self.Files = [os.path.basename(os.path.dirname(f)) for f in self.img_name_list]

        # 画像を読み込む
        self.img_list = self.load_imgs(self.img_name_list) if (img_list is None) else img_list

        # UIの初期化
        self.initUI()

        # ファイルを配置
        self.set_FileList()

        # 画像
        self.FileName = self.FileList.item(0).text()
        self.idx = 0

        # 画像を読み込む
        self.load_ImageFile()

        # Figureの初期化
        self.initFigure()

        # ファイルをクリックしたとき
        self.FileList.currentRowChanged.connect(self.listMove)

        # ファイルを変更した時のイベント
        #self.FileList.itemSelectionChanged.connect(self.FileList_Changed)
        pass
    def listMove(self, item):
        #selected_idx = self.FileList.currentRow()
        # 画像を取り出す
        img = self.img_list[item]

        # Figure を更新
        self.update_Figure(img)
        #item.setBackground(QtGui.QColor('red')) #クリックされた行の色変更
        #print(a) #こっちは逆にitemから行番号が取得できないからこうした
        return

    # UIの初期化
    def initUI(self):
        # Figure用のWidget
        self.FigureWidget = QtWidgets.QWidget(self)
        # FigureWidgetにLayoutを追加
        self.FigureLayout = QtWidgets.QVBoxLayout(self.FigureWidget)
        # Marginを消す
        self.FigureLayout.setContentsMargins(0, 0, 0, 0)

        # ファイルのリスト
        self.FileList = QtWidgets.QListWidget(self)

        # 配置
        # 一番外枠のウィンドウ位置
        self.setGeometry(x, y, window_x, window_y)
        # FigureWidget のウィンドウ位置（ウィンドウサイズ 500×600）
        self.FigureWidget.setGeometry(flist_x, 0, window_x - flist_x, window_y)
        self.FileList.setGeometry(0, 0, flist_x, window_y)
        pass

    # Figureの初期化
    def initFigure(self):
        # Figureを作成
        self.Figure = plt.figure()
        # FigureをFigureCanvasに追加
        self.FigureCanvas = FigureCanvas(self.Figure)
        # LayoutにFigureCanvasを追加
        self.FigureLayout.addWidget(self.FigureCanvas)

        self.axis = self.Figure.add_subplot(1, 1, 1)
        self.axis_img = self.axis.imshow(self.image, cmap='gray')
        plt.axis('off')
        pass

    # ファイルを配置
    def set_FileList(self):
        # ファイルリストに追加
        for img_name in self.Files:
            self.FileList.addItem(img_name)
        pass

    # 画像ファイルを読み込む
    def load_imgs(self, img_name_list):
        re_img_list = []

        for i in img_name_list:
            img = Image.open(i)
            img = np.asarray(img)
            re_img_list.append(img)
            pass
        return re_img_list

    # 画像ファイルを読み込む
    def load_ImageFile(self):
        # 画像を開く
        image = self.img_list[self.idx]

        # numpy.ndarrayに
        self.image = np.asarray(image)
        return

    # ファイルを変更した時の関数
    def FileList_Changed(self):
        # 選択された
        selected_idx = self.FileList.SelectedClicked
        # 選択しているファイルの名前を取得
        a = self.FileList
        b = self.FileList.selectedItems()
        c = self.FileList.selectedItems()[0]
        d = self.FileList.selectedItems()[0].text()

        selected_name = self.FileList.selectedItems()[0].text()

        # ファイル名のインデックスを取得
        self.idx = self.show_fname_list.index(selected_name)

        # 画像を取り出す
        img = self.img_list[self.idx]

        # Figure を更新
        self.update_Figure(img)
        pass

    # Figureを更新
    def update_Figure(self, img):
        self.axis_img.set_data(img)
        self.FigureCanvas.draw()
        pass



def main():
    QApp = QtWidgets.QApplication(sys.argv)
    app = Application(0)
    app.show()
    sys.exit(QApp.exec_())
    pass


if __name__ == '__main__':
    main()