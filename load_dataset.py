"""

matlab ファイルの解析

"""
from keras.utils import np_utils

import numpy as np
import pickle

import load_mat
from initialize.init_global import *



#
def __load_pickle(file_name):
    # サブキャリア数サイズの np.array がサンプル数分入ったリスト
    # [(64, )... (64, )]
    with open(file_name, 'rb') as f:
        data = pickle.load(f, encoding='latin1')

    # 20190720 追加（なぜかリスト内の要素が欠けてた）
    for l in data:
        s = len(l)
        add_len = sub_carrier - s
        if (add_len > 0):
            add_data = [0j] * add_len
            l.extend(add_data)
        elif (add_len < 0):
            del l[sub_carrier:]
        pass

    # (サンプル数、サブキャリア数)
    data = np.array(data)

    # チャネル方向に次元を増やす
    if (antenna == 1):
        data = data[:, :, np.newaxis]

    return data


# mat ファイルを読み込んで学習データを返す
# 引数：ファイル名
# 戻り値：振幅と位相
""" 1 ファイル 1 データ """
def __load_data_file(file_name, mat_flag=False):
    # mat ファイルを読み込む
    if (mat_flag):
        mat_data = load_mat.load_mat_file(file_name)

        # データを取り出す（データを表すキー）
        data = load_mat.get_mat_value(mat_data, "csimat")
    else:
        data = __load_pickle(file_name)
        pass

    """ 1 ファイルのデータを複数個に分割する """
    # サンプル、サブキャリア数、アンテナ
    t, s, a = data.shape
    re_data_list = []

    for i in range(int(t / gesture_per_sample)):
        # 20（gesture_per_sample） ずつ区切る
        tmp_data = data[i * gesture_per_sample: (i + 1) * gesture_per_sample, :, :]

        # リストにデータを追加
        re_data_list.append(tmp_data)
        pass

    return re_data_list



"""
folder
-0
--mat_file_0
--mat_file_1
--...
-1
-...
"""
# フォルダ内のサブフォルダからデータを読み込む
# 引数：フォルダ名
# 戻り値：データ、ラベル
def __load_data_infolder(folder):
    """
    フォルダ内のサブフォルダごとにデータを取得
    """
    data_list = []
    label_list = []
    for i in classes:
        # 各ジェスチャのフォルダ名を作成
        dir_name = os.path.join(folder, str(i))
        # 中身のファイル名を取得
        f_name_list = os.listdir(dir_name)

        # データを読み込み
        for f_name in f_name_list:
            # ファイル名を取得
            f_name = os.path.join(dir_name, f_name)

            """ データの整形をここでする """
            # データ（リスト）を読み込む
            tmp_data_list = __load_data_file(f_name)

            # ラベルを割り当てる
            label = [classes.index(i)]
            # データリストの分だけラベルを入れる
            label = label * len(tmp_data_list)

            # データとラベルを追加（どっちもリストの追加）
            data_list.extend(tmp_data_list)
            label_list.extend(label)
            pass
        pass

    return data_list, label_list


# http://pynote.hatenablog.com/entry/numpy-complex-functions
# numpy 配列（複素数）から絶対値を返す
# 引数：複素数の配列
# 戻り値：振幅の配列（shape は同じ）
def re_abs(mat_value):
    value = np.abs(mat_value)

    return value

# numpy 配列（複素数）から位相を返す
# 引数：複素数の配列
# 戻り値：振幅の配列（shape は同じ）
def re_angle(mat_value):
    re_angle = np.angle(mat_value)

    # 0～360 度に変換する場合
    #re_degrees = np.degrees(np.angle(mat_value))

    return re_angle

# 引数：データ
# 戻り値：データ
def data_processing(data, concat_axis=3):
    # 振幅と位相に分ける
    data_abs = re_abs(data)
    data_angle = re_angle(data)

    # チャネル方向にコンカチ
    # (*, *, 30, 3) → (*, *, 30, 6)
    re_data = np.concatenate([data_abs, data_angle], axis=concat_axis)

    return re_data

# フォルダ内にあるラベル名のフォルダからデータを読み込む
def re_dataset(folder, concat_axis=3):
    # フォルダからデータとラベルを読み込んでリストにまとめる
    data_list, label_list = __load_data_infolder(folder)

    # データの形式を変える
    data_list = np.array(data_list)
    label_list = np.array(label_list)

    # データに対する前処理
    data_list = data_processing(data_list, concat_axis=concat_axis)
    # label を one-hot-vector に変換（kfold を使うときに整数しか split できなかった）
    #label_list = np_utils.to_categorical(label_list, num_classes=len(classes))

    print(data_list.shape)
    print(label_list.shape)

    return data_list, label_list




def main():
    pass


if __name__ == '__main__':
    main()
