"""

パラメータ調整
グリッドサーチする

"""

# 結果の名前
ex_word = "test_20190721"

test_flag = True
test_flag = False


# サーチするパラメータ名
search_p_list = ["model", "batch_size", "lr", "optimizer", "concat_axis"]

# ネットワーク構造
p_architecture = [0, 1]
p_architecture = [1]

# バッチサイズ
p_batch_size = [16, 64, 256]
#p_batch_size = [8, 16, 64]

# 学習率
p_lr =[1e-3, 1e-4]
p_lr =[1e-3, 1e-4, 1e-5]

# オプティマイザ
p_optimizer = ["Adam", "SGD"]
p_optimizer = ["Adam"]

# 連結軸
# load_dataset.__data_processing(data) で使う
p_concat_axis = [2, 3]
p_concat_axis = [3]

if (test_flag):
    p_architecture = [1]
    p_batch_size = [64]
    p_lr = [1e-3]
    p_optimizer = ["Adam"]
    p_concat_axis = [2]
    pass


search_p_dict = {"model": p_architecture,
                 "batch_size": p_batch_size,
                 "lr": p_lr,
                 "optimizer": p_optimizer,
                 "concat_axis": p_concat_axis}



def main():
    pass


if __name__ == '__main__':
    main()