"""

パラメータ決定後に学習した model を保存する

"""

from keras.layers import Input, Dense, Reshape, Flatten, Dropout, Add
from keras.layers import BatchNormalization, Activation, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.utils import np_utils
from keras.layers import Concatenate
from keras.layers import Lambda
from keras.layers import MaxPooling2D, AveragePooling2D
from keras import initializers
from keras.callbacks import LearningRateScheduler

# 混合行列を求められる
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold

import network_compile
import build_model
import fit_visualize
import load_dataset
import calculate_result

from initialize.init_global import *
from _init_setting import *
import numpy as np
import argparse




def train(model_func, concat_axis, optimizer, lr, batch_size, Result_Dir,
          train_data, train_label, val_data, val_label):
    def step_decay(epoch):
        x = lr
        if epoch >= 50: x /= 10
        return x

    # 入力形式(200, 60, 3) or (20, 30, 6)
    input_shape = (sample, 2 * sub_carrier, antenna)
    if (concat_axis == 3): input_shape = (sample, sub_carrier, 2 * antenna)

    train_label = np_utils.to_categorical(train_label, num_classes=len(classes))
    val_label = np_utils.to_categorical(val_label, num_classes=len(classes))

    ckbk = []
    lr_decay = LearningRateScheduler(step_decay)
    ckbk.append(lr_decay)

    # モデルを構築
    model = model_func(input_tensor=Input(shape=input_shape), output_channels=num_gesture)
    # モデルを書き出す
    network_compile.print_summary(model, Result_Dir.summary_file_name)
    # ネットワークコンパイル
    model = network_compile.compile_signfi(model, optimizer, lr)

    """ 学習 """
    history = model.fit(
        x=train_data, y=train_label,
        batch_size=batch_size, epochs=epochs,
        verbose=1, callbacks=ckbk,
        validation_data=(val_data, val_label)
    )

    cm_l_t, cm_p_t = [], []
    cm_l_v, cm_p_v = [], []

    # 学習データで混合行列を作る
    _label, _pred = calculate_result.re_preparation_cm(model, data=train_data, label=train_label)
    cm_l_t.append(_label)
    cm_p_t.append(_pred)
    # テストデータで混合行列を作る
    _label, _pred = calculate_result.re_preparation_cm(model, data=val_data, label=val_label)
    cm_l_v.append(_label)
    cm_p_v.append(_pred)

    # 混合行列を作成
    _label = np.concatenate(cm_l_t, axis=0)
    _pred = np.concatenate(cm_p_t, axis=0)
    calculate_result.make_confusion_matrix(
        save_name=Result_Dir.cm_train_name, label=_label, pred=_pred, classes=classes
    )

    # 検証データで混合行列を作成
    _label = np.concatenate(cm_l_v, axis=0)
    _pred = np.concatenate(cm_p_v, axis=0)
    calculate_result.make_confusion_matrix(
        save_name=Result_Dir.cm_test_name, label=_label, pred=_pred, classes=classes
    )

    """ 学習後の処理 """
    # history をファイルに保存
    fit_visualize.save_history_txt(history, Result_Dir.history_file_name)

    # history 可視化
    fit_visualize.make_learning_curve(Result_Dir.history_file_name,
                                      Result_Dir.accuracy_file_name,
                                      Result_Dir.loss_file_name)

    # model を保存
    model.save(Result_Dir.save_model_name)

    return


def main():
    # ArgumentParserの使い方を簡単にまとめた
    # https://qiita.com/kzkadc/items/e4fc7bc9c003de1eb6d0
    # parser を作る
    parser = argparse.ArgumentParser()
    # 3. parser.add_argumentで受け取る引数を追加していく
    parser.add_argument("--data", type=str, default=pub_folder, help="")
    parser.add_argument("--model", type=int, default=1, help="model architecture")
    parser.add_argument("--batch_size", type=int, default=64, help="batch size")
    parser.add_argument("--lr", type=float, default=1e-3, help="batch size")
    parser.add_argument("--optimizer", type=str, default="Adam", help="optimizer")
    parser.add_argument("--concat_axis", type=int, default=3, help="batch size")
    parser.add_argument("--root", type=str, default=ex_word, help="save")

    # 4. 引数を解析
    args = parser.parse_args()

    # パラメータ類
    model = args.model
    batch_size = args.batch_size
    lr = args.lr
    optimizer = args.optimizer
    concat_axis = args.concat_axis

    train_dir = ""
    val_dir = ""
    sum_dir = args.data
    fold_flag = True


    search_p_dict = {"model": model,
                     "batch_size": batch_size,
                     "lr": lr,
                     "optimizer": optimizer,
                     "concat_axis": concat_axis}
    # 結果を入れるフォルダ
    result_dir = Result_Dir(root=args.root,
                            search_p_key=search_p_list,
                            search_p_dict=search_p_dict,
                            makedir_flag=True)

    # モデルを定義
    model_func = build_model.call_build_model(p_model=model)
    # データとラベルを取り出す
    if (fold_flag):
        # データとラベルを取り出す
        data, label = load_dataset.re_dataset(args.data, concat_axis=concat_axis)
        # k 交差検証
        kfold = StratifiedKFold(n_splits=n_kfold, shuffle=True, random_state=1)
        for t, test in kfold.split(data, label):
            train_data, train_label = data[t], label[t]
            val_data, val_label = data[test], label[test]
            break
        pass
    else:
        train_data, train_label = load_dataset.re_dataset(train_dir, concat_axis=concat_axis)
        val_data, val_label = load_dataset.re_dataset(val_dir, concat_axis=concat_axis)
        pass

    # 学習
    train(model_func, concat_axis, optimizer, lr, batch_size, result_dir,
          train_data, train_label, val_data, val_label)

    return


if __name__ == '__main__':
    main()
    pass